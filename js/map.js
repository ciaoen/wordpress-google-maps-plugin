jQuery(document).ready(function ($) {

    $('.add-map').on('submit', function () {
        return false;
    });

    $('.pattern img').on('click', function () {

        $('.pattern img').removeClass('active');
        $(this).addClass('active');
        var patternId = $(this).attr('pattern-number');
        $('#color').val(patternId);

        getMapInfo(patternId);

    });

    // Edit Map
    $('.edit-map').on('submit', function () {

        var id = $(this).find('.number').val();

        window.location = "admin.php?page=add-map&edit=" + id + "";

        return false;
    });

    // Delete Map
    $('.delete-map').on('submit', function () {

        var form = $(this);

        $.ajax({
            type: "POST",
            url: ajaxurl,
            data: {
                action: "delete_map",
                id: $(this).find('.number').val(),
            },
            success: function (data) {
                var obj = JSON.parse(data);

                if (obj.error == 0) {
                    form.closest('tr').remove();
                }
            }
        });
        return false;
    });

    function getMapInfo(color) {
        $.ajax({
            type: "POST",
            url: ajaxurl,
            data: {
                action: "preview_map",
                address: $('#address').val(),
                color: color,
            },
            success: function (data) {
                var obj = JSON.parse(data);

                if (obj.error == 0) {

                    var width = ( $('#map-width').val() != 0 ) ? $('#map-width').val() : 611;
                    var height = ( $('#map-height').val() != 0 ) ? $('#map-height').val() : 500;
                    $('#map').css('width', width + 'px');
                    $('#map').css('height', height + 'px');
                    mapInit(obj.lat, obj.long, obj.color);
                }
            }
        });
        return false;
    }

    $('.save-map').on('click', function () {

        var id = '';

        if (getParameterByName('edit')) {
            id = getParameterByName('edit');
        }

        $.ajax({
            type: "POST",
            url: ajaxurl,
            data: {
                action: "save_map",
                address: $('#address').val(),
                color: $('#color').val(),
                name: $('#map-name').val(),
                width: $('#map-width').val(),
                height: $('#map-height').val(),
                text: $('#map-text').val(),
                id: id
            },
            success: function (data) {
                var obj = JSON.parse(data);

                if (obj.error == 0) {
                    $('#address').val('');
                    $('#color').val('');
                    window.location = "admin.php?page=show-map";
                }
            }
        });
        return false;
    });

    //get parameters by url
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function initialize(lat, long, color) {

        if (color == 'none' || color == '') {
            var styles = [];
        }
        else {
            var styles = jQuery.parseJSON(color);
        }

        var myLatlng = new google.maps.LatLng(lat, long);

        var styledMap = new google.maps.StyledMapType(styles,
            {name: "Styled Map"});

        var mapOptions = {
            zoom: 13,
            center: myLatlng,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            }
        }

        var input = ( document.getElementById('address'));

        if (input.value == '' && getParameterByName('page') == 'add-map') {
            geocoder.geocode({'latLng': myLatlng}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        $('#address').val(results[0].formatted_address);
                    }
                }
            });
        }
        var autocomplete = new google.maps.places.Autocomplete(input, {types: ['geocode']});

        autocomplete.addListener('place_changed', function () {
            var color = $('#color').val();
            getMapInfo(color);
        });

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'This is a caption'
        });

        if ($('#map-text').val() != '') {
            var infoWindow = new google.maps.InfoWindow({
                content: $('#map-text').val()
            });

            google.maps.event.addListener(marker, "click", function (e) {
                infoWindow.open(map, this);
            });
            infoWindow.open(map, marker);
        }

    }

    function mapInit(lat, long, color) {
        google.maps.event.addDomListener(window, 'load', initialize(lat, long, color));
    }

    if (getParameterByName('edit')) {
        var color = $('#color').val();
        getMapInfo(color);
    }
    else if (getParameterByName('page') == 'add-map' && getParameterByName('edit') == '') {

        var geocoder = new google.maps.Geocoder();

        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(
                function (position) {

                    if (typeof position != 'undefined') {
                        var lat = position.coords.latitude;
                        var long = position.coords.longitude;
                        var color = $('#color').val();

                    }

                    google.maps.event.addDomListener(window, 'load', initialize(lat, long, color));
                    return false;

                }, function (error) {

                    var lat = 40.748817;
                    var long = -73.985428;
                    var color = $('#color').val();

                    google.maps.event.addDomListener(window, 'load', initialize(lat, long, color));

                });
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    }

    $('#map-width, #map-height, #map-text').focusout(function () {
        var color = $('#color').val();
        getMapInfo(color);
    });

});